# fsl_heal

use enantiomorphic technique to fill pathological sections of a 3d brain image. Requires a binary mask with ones in areas of damage. 

## usage

```
usage: fsl_heal img.nii.gz mask.nii.gz

fsl_heal removes tissue damage from a 3D brain image. Relies on fslpython
being on $PATH

positional arguments:
  img         path to the image to heal
  mask        path to the mask

optional arguments:
  -h, --help  show this help message and exit
  -out OUT    optional path to save result to. Default is None and will add
              the suffix _healed
  -v          verbose mode. Show all debug messages
  ```

  ## example CT

  ![example healing 1](./pictures/example1.png)

  ## Notes

  For now, `fsl_heal` uses FSL's flirt for linear registration between the image and its reflection. A planned change is to switch to non-linear registration using fnirt. 

  fnirt will be used with a custom config file developed by Rick Lange. The custom config removes all of the high resolution stages of fnirt, and therefore runs much faster than the default nonlinear registraion. Intensity normalisation and bias correction have also been removed since both images (input, and ref) are from the same brain in the case of fsl_heal. 