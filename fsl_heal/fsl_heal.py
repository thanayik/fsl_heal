#!/usr/bin/env fslpython
"""
use enantiomorphic technique to fill pathological sections of a 3d brain image.
Requires a binary mask with ones in areas of damage.
"""
import os
import sys
import argparse
import subprocess
import logging

import nibabel as nii
import numpy as np
from fsl.utils import path as fslpath
from fsl.data.image import ALLOWED_EXTENSIONS
from fsl.wrappers import flirt as fslflirt

FSLDIR = os.getenv("FSLDIR")

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)

parser = argparse.ArgumentParser(description="fsl_heal removes tissue damage from a 3D brain image. Relies on fslpython being on $PATH ",
usage="fsl_heal img.nii.gz mask.nii.gz")
parser.add_argument('img', type=str, help='path to the image to heal')
parser.add_argument('mask', type=str, help='path to the mask')
parser.add_argument('-out', type=str, help='optional path to save result to. Default is None and will add the suffix _healed', default=None)
parser.add_argument('-dof', type=int, help='optional dof to pass to FSL flirt registration. Default it 6', default=6)
parser.add_argument('-v', help='verbose mode. Show all debug messages', action='store_true')


def mirror(in_pth, out_pth=None):
    """
    mirror an image and save it
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_mirror.nii.gz'
    
    LOG.debug('reflecting input image...')

    in_img = nii.load(in_pth)
    in_data = in_img.get_fdata()

    mirror_data = np.flipud(in_data)

    mirror_img = nii.Nifti1Image(mirror_data, in_img.affine, in_img.header)
    nii.save(mirror_img, out_pth)
    return out_pth

def heal(in_pth, mask_pth, out_pth=None, cleanup=False, dof=6):
    """
    heal an input image with information from the opposite hemisphere using a mask image
    """
    remove_list = []

    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_healed.nii.gz'
    LOG.debug('out_pth is {}'.format(out_pth))
    
    mirror_img = mirror(in_pth)
    LOG.debug('mirror_pth is {}'.format(mirror_img))
    if cleanup:
        remove_list.append(mirror_img)

    mirror_reg = fslpath.removeExt(mirror_img, allowedExts=ALLOWED_EXTENSIONS) + '_reg.nii.gz'
    if cleanup:
        remove_list.append(mirror_reg)

    _ = fslflirt(mirror_img, in_pth, out=mirror_reg, dof=dof)

    in_img = nii.load(in_pth)
    in_data = in_img.get_fdata()
    orig_shape = in_data.shape

    in_data_flat = in_data.flatten()

    mask_img = nii.load(mask_pth)
    mask_data = mask_img.get_fdata().flatten()

    mirreg_img = nii.load(mirror_reg)
    mirreg_data = mirreg_img.get_fdata().flatten()

    out_data_flat = (in_data_flat * (1.0-mask_data)) + (mirreg_data * mask_data)
    out_data = np.reshape(out_data_flat, orig_shape)

    out_img = nii.Nifti1Image(out_data, in_img.affine, in_img.header)
    nii.save(out_img, out_pth)


def main():
    """
    main program to execute from command line
    """
    args = parser.parse_args()
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    LOG.addHandler(ch)
    if args.v:
        LOG.setLevel(logging.DEBUG)

    if FSLDIR is None:
        LOG.info('unable to get FSLDIR. Make sure you have your $FSLDIR environment variable set to a valid FSL install path')

    heal(args.img, args.mask, out_pth=args.out, dof=args.dof)

if __name__ == "__main__":
    sys.exit(main())
